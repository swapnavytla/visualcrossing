package visualcrossing.selenium.framework.pages;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

/**
 * WeatherDataPage.
 * 
 * @author Swapna.Vytla.
 *
 */
public class WeatherDataPage extends BaseAbstractPage {

	public static final WeatherDataPage INSTANCE = new WeatherDataPage();

	protected static final HomePage HOME_PAGE = HomePage.INSTANCE;

	protected static final WeatherForecastPage FORECAST_PAGE = WeatherForecastPage.INSTANCE;

	private String url = WeatherDataTestConstants.WEATHERDATA_URL;
	private String title = WeatherDataTestConstants.WEATHERDATA_TITLE;

	/**
	 * Weather Data Page.
	 */
	public WeatherDataPage() {
		// Empty Constructor
	}

	public String getUrl() {
		return this.url;
	}

	public String getTitle() {
		return this.title;
	}

	// --------XPATH - WEATHER DATA PAGE ELEMENTS-------- //

	@FindBy(xpath = "//*[@id='wxlocation']")
	private WebElement locationText;

	@FindBy(xpath = "//*[@id='wxdataform']/div[2]/div/button")
	private WebElement historyForecastDropdownBtn;

	@FindBy(xpath = "//*[@id='wxdataform']/div[2]/div/ul/li/a/span[contains(text(),'Forecast')]")
	private WebElement forecastOption;

	@FindBy(xpath = "//*[@id='wxdataform']/div[2]/div/ul/li[1]/a/span[contains(text(),'History')]")
	private WebElement historyOption;

	@FindBy(xpath = "//*[@id='wxdataform']/div[2]/button")
	private WebElement searchButton;

	// -------- PUBLIC METHODS FOR USER ACTIONS -------- //

	/**
	 * Click Search button.
	 */
	public void clickSearchButton() {
		waitUntilVisible(searchButton);
		searchButton.click();
	}

	// ----- PUBLIC METHODS FOR HELPER ------ //

	/**
	 * Setup before class initialization.
	 * 
	 */
	protected void setupBeforeClassWeatherDataTest() {
		initWeatherDataTest();
	}

	/**
	 * Navigate to Weather Data home page.
	 */
	protected void initWeatherDataTest() {
		initTest();
		acceptCookies();
	}

	/**
	 * Initialize browser and goes to home page.
	 */
	private void initTest() {
		openBrowser();
		maximizeWindow();
		HOME_PAGE.reInitializePageElementsForCurrentBrowser();
		enterUrl(url);
		waitforPageLoad();
		assertEquals(driver.getTitle(), getTitle());
	}

	/**
	 * Accepts cookies and stays in home page.
	 */
	private void acceptCookies() {
		HOME_PAGE.clickacceptCookies();
	}

	/**
	 * Navigate to Weather Data Page
	 */
	protected void navigateToWeatherDataPage() {
		HOME_PAGE.clickWeatherData();
	}

	/**
	 * Setup before method initialization.
	 * 
	 */
	protected void setupBeforeMethodWeatherDataTest() {
		navigateToWeatherDataPage();
	}

	/**
	 * Fill out all needed values in the page.
	 * 
	 * @param weatherDataInfo WeatherDataInfo
	 * @return this WeatherDataPage
	 */
	public WeatherDataPage selectForecastOptionAndSearchWeatherData(WeatherDataInfo weatherDataInfo) {
		if (weatherDataInfo.getLocation() != null) {
			clearAndEnterText(locationText, weatherDataInfo.getLocation());
		}
		selectDropdownOption(historyForecastDropdownBtn, forecastOption);
		return this;
	}

	/**
	 * Fill out all needed values in the page.
	 * 
	 * @param weatherDataInfo WeatherDataInfo
	 * @return this WeatherDataPage
	 */
	public WeatherDataPage searchWeatherForecastData(WeatherDataInfo weatherDataInfo) {
		if (weatherDataInfo.getLocation() != null) {
			clearAndEnterText(locationText, weatherDataInfo.getLocation());
		}
		return this;
	}

}
