package visualcrossing.selenium.framework.pages;

/**
 * Data holder for Weather Data Page.
 * 
 * @author Swapna Vytla
 * 
 */

public class WeatherDataInfo {

	private static final WeatherDataInfo INSTANCE = new WeatherDataInfo();

	private String location;

	public WeatherDataInfo() {
		withDefaultValues();
	}

	public static WeatherDataInfo getInstance() {
		return INSTANCE;
	}

	/**
	 * Get all default values Weather Data page.
	 *
	 * @return object Weather Data Info
	 */
	public WeatherDataInfo withDefaultValues() {
		location = "Hyderabad";
		return this;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

}
