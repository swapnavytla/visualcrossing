package visualcrossing.selenium.framework.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

/**
 * WeatherForecastPage.
 * 
 * @author Swapna.Vytla.
 *
 */
public class WeatherForecastPage extends BaseAbstractPage {

	public static final WeatherForecastPage INSTANCE = new WeatherForecastPage();
	private String url = WeatherDataTestConstants.WEATHERHISTORY_URL;
	private String title = WeatherDataTestConstants.WEATHERHISTORY_TITLE;

	/**
	 * Weather Forecast Home Page.
	 */
	public WeatherForecastPage() {
		// Empty Constructor
	}

	public String getUrl() {
		return this.url;
	}

	public String getTitle() {
		return this.title;
	}

	// --------XPATH - ELEMENTS-------- //

	@FindBy(xpath = "//*[@id='locationDropdownMenuButton']")
	private WebElement weatherForecastCityName;

	@FindBy(xpath = "/html/body/main/section[2]/div/div[1]/a[contains(@class,'active')and contains(@href,'weather-forecast')]")
	private WebElement fifteenDayForecastOptionIsSelected;

	@FindBy(xpath = "/html/body/main/section[2]/div/div[1]/a[contains(@href,'weather-forecast')]")
	private WebElement fifteenDayForecastOption;

	// -------- PUBLIC METHODS FOR USER ACTIONS -------- //

	/**
	 * Click ForeCast Option button.
	 */
	public void selectFifteenDayForecast() {
		waitUntilVisible(fifteenDayForecastOption);
		fifteenDayForecastOption.click();
	}

	/**
	 * Get the text of weather forecast city name.
	 */
	public String getTextOfWeatherForcastCityName() {
		return getText(weatherForecastCityName);
	}

	/**
	 * Check wheather forecast option is selected.
	 */
	public boolean checkIfForecastOptionIsSelected() {
		return fifteenDayForecastOptionIsSelected != null;

	}

	/**
	 * Check wheather forecast option is not selected.
	 */
	public boolean checkIfForecastOptionIsNotSelected() {
		return fifteenDayForecastOption != null;

	}

}
