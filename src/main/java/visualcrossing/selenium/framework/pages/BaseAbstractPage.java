package visualcrossing.selenium.framework.pages;

import java.time.Duration;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.PageFactory;

import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;

import org.openqa.selenium.support.ui.Wait;

/**
 * Abstract class with common methods for WeatherForecast Selenium Pages.
 *
 * @author Swapna Vytla
 */
public abstract class BaseAbstractPage {

	protected static WebDriver driver;

	/**
	 * Launch google chrome browser.
	 * 
	 */
	protected void openBrowser() {
		System.setProperty("webdriver.chrome.driver", "D:\\WeatherForecast1.0\\visualcrossing\\src\\main\\resources\\chromedriver\\chromedriver.exe");
		driver = new ChromeDriver();
	}

	/**
	 * Close browser.
	 * 
	 */
	protected void closeBrowser() {
		driver.quit();
	}

	/**
	 * Maximize browser window.
	 * 
	 */
	protected static void maximizeWindow() {
		driver.manage().window().maximize();
	}

	/**
	 * Wait for page to load.
	 * 
	 */
	protected static void waitforPageLoad() {
		driver.manage().timeouts().pageLoadTimeout(Duration.ofMinutes(5));
	}

	/**
	 * Enter url.
	 * 
	 */
	protected static void enterUrl(String url) {
		driver.get(url);
	}

	/**
	 * Click on the Webelement.
	 * 
	 */
	protected void click(WebElement element) {
		JavascriptExecutor executor = (JavascriptExecutor) driver;
		executor.executeScript("arguments[0].click();", element);
	}

	/**
	 * ReInitialize PageElements For Current Browser.
	 * 
	 */
	public void reInitializePageElementsForCurrentBrowser() {
		PageFactory.initElements(driver, this);
	}

	/**
	 * Get the text of a webelement when it is visible.
	 * 
	 * @param we WebElement
	 * @return text of a webelement
	 */
	protected String getText(WebElement we) {
		waitUntilVisible(we);
		return we.getText();
	}

	/**
	 * To send values in the TextBoxes.
	 * 
	 * @param textBox {@link WebElement} which represents text box
	 * @param text    text which needs to be typed in the text box.
	 * 
	 */
	protected void clearAndEnterText(WebElement textBox, String text) {
		waitUntilVisible(textBox);
		textBox.click();
		textBox.clear();
		textBox.sendKeys(text + Keys.TAB);
	}

	protected void selectDropdownOption(WebElement selectBox, WebElement optionToChoose) {
	    waitUntilVisible(selectBox);
		selectBox.click();
		waitUntilVisible(optionToChoose);
	    optionToChoose.click();
	}

	/**
	 * To wait until a web element is visible in UI.
	 * 
	 * @param we {@link WebElement} which represents the element which expected to
	 *           be visible
	 * 
	 */
	protected void waitUntilVisible(WebElement we) {
		Wait<WebDriver> wait = newFluentWait();
		wait.until(ExpectedConditions.visibilityOf(we));
	}

	private Wait<WebDriver> newFluentWait() {
		// Fluentwait: this expression poll every 100 ms with a timeout of 10 seconds.
		Wait<WebDriver> wait = (new FluentWait<WebDriver>(driver)).withTimeout(Duration.ofSeconds(10L))
				.pollingEvery(Duration.ofMillis(100L)).ignoring(NoSuchElementException.class);
		return wait;
	}

}
