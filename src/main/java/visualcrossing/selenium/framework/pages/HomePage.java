package visualcrossing.selenium.framework.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

/**
 * WeatherDataHomePage.
 * 
 * @author Swapna.Vytla.
 *
 */
public class HomePage extends BaseAbstractPage {

	public static final HomePage INSTANCE = new HomePage();
	private String url = WeatherDataTestConstants.HOMEPAGE_URL;
	private String title = WeatherDataTestConstants.HOMEPAGE_TITLE;

	/**
	 * Weather Data Home Page.
	 */
	public HomePage() {
		// Empty Constructor
	}

	public String getUrl() {
		return this.url;
	}

	public String getTitle() {
		return this.title;
	}

	// --------XPATH - WEATHER HOMEPAGE ELEMENTS-------- //

	@FindBy(xpath = "// *[@id='errorModal']/div/div/div[2]/div[2]/button[1]")
	private WebElement acceptCoockiesButton;

	@FindBy(linkText = "Weather Data")
	private WebElement weatherDataLink;

	// -------- PUBLIC METHODS FOR USER ACTIONS -------- //

	/**
	 * Click AcceptCookies button.
	 */
	public void clickacceptCookies() {
		waitUntilVisible(acceptCoockiesButton);
		click(acceptCoockiesButton);
	}

	/**
	 * Click Weather Data link.
	 */
	public void clickWeatherData() {
		waitUntilVisible(weatherDataLink);
		weatherDataLink.click();
	}

}
