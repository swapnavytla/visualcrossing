package visualcrossing.selenium.framework.pages;

/**
 * List of constants to be used in tests of weather Data Features.
 * 
 * @author Swapna Vytla
 */
public abstract class WeatherDataTestConstants {

	/**
	 * Constant for HOMEPAGE_URL.
	 */
	public static final String HOMEPAGE_URL = "https://www.visualcrossing.com";

	/**
	 * Constant for HOMEPAGE_TITLE.
	 */
	public static final String HOMEPAGE_TITLE = "Weather Data & Weather API | Visual Crossing";

	/**
	 * Constant for WEATHERDATA_URL.
	 */
	public static final String WEATHERDATA_URL = "https://www.visualcrossing.com/weather-data";

	/**
	 * Constant for WEATHERDATA_TITLE.
	 */
	public static final String WEATHERDATA_TITLE = "Historical Weather Data & Weather Forecast Data | Visual Crossing";

	/**
	 * Constant for WEATHERHISTORY_URL.
	 */
	public static final String WEATHERHISTORY_URL = "https://www.visualcrossing.com/weather-history/Hyderabad";

	/**
	 * Constant for WEATHERHISTORY_TITLE.
	 */
	public static final String WEATHERHISTORY_TITLE = "Historical weather data for Hyderabad | Visual Crossing";

	/**
	 * Constant for WEATHER_HISTORY_CITYNAME.
	 */
	public static final String WEATHER_FORECAST_CITYNAME = "Hyderabad";

}
