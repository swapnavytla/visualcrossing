package visualcrossing.selenium.tests;

import static org.testng.Assert.*;

import org.testng.annotations.*;

import visualcrossing.selenium.framework.pages.WeatherDataInfo;
import visualcrossing.selenium.framework.pages.WeatherDataPage;
import visualcrossing.selenium.framework.pages.WeatherDataTestConstants;

/**
 * Test for Weather Forecast functionality.
 * 
 * @author Swapna Vytla
 */
public class WeatherForecastTest extends WeatherDataPage {

	private static final WeatherDataPage WEATHER_DATA_PAGE = WeatherDataPage.INSTANCE;

	/**
	 * Setup before class initialization.
	 */
	@BeforeClass
	public void setupBeforeClass() {
		setupBeforeClassWeatherDataTest();
	}

	/**
	 * Setup before test initialization.
	 */
	@BeforeMethod
	public void setupBefore() {
		setupBeforeMethodWeatherDataTest();
		getPage().reInitializePageElementsForCurrentBrowser();
		getInfo();
	}

	@Test(priority = 1)
	public void searchWeatherForeCastDataOfGivenLocation() {
		searchWeatherForecastOfCity();
		FORECAST_PAGE.reInitializePageElementsForCurrentBrowser();
		assertTrue(FORECAST_PAGE.checkIfForecastOptionIsNotSelected());
		FORECAST_PAGE.selectFifteenDayForecast();
		assertEquals(FORECAST_PAGE.getTextOfWeatherForcastCityName(),
				WeatherDataTestConstants.WEATHER_FORECAST_CITYNAME);
	}

	@Test(priority = 2)
	public void searchWeatherForeCastDataBySelectingForeCastOption() {
		searchWeatherForecastOfCityBySelectingOption();
		FORECAST_PAGE.reInitializePageElementsForCurrentBrowser();
		assertTrue(FORECAST_PAGE.checkIfForecastOptionIsSelected());
		assertEquals(FORECAST_PAGE.getTextOfWeatherForcastCityName(),
				WeatherDataTestConstants.WEATHER_FORECAST_CITYNAME);
	}

	/**
	 * It executes once and after the execution of the last test.
	 */
	@AfterClass
	public void setupAfterClass() {
		closeBrowser();
	}

	/**
	 * Get a WeatherDataInfo instance.
	 */
	protected WeatherDataInfo getInfo() {
		return WeatherDataInfo.getInstance();
	}

	/**
	 * Gets the page instance.
	 */
	protected WeatherDataPage getPage() {
		return INSTANCE;
	}

	/**
	 * Fill location input field, select search option and click on search button.
	 */
	protected void searchWeatherForecastOfCityBySelectingOption() {
		getPage().selectForecastOptionAndSearchWeatherData(getInfo().withDefaultValues());
		getPage().clickSearchButton();
	}

	/**
	 * Fill location input field, and click on search button.
	 */
	protected void searchWeatherForecastOfCity() {
		getPage().searchWeatherForecastData(getInfo().withDefaultValues());
		getPage().clickSearchButton();
	}

}
